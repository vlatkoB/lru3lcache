{-# LANGUAGE DataKinds, DerivingStrategies, QuasiQuotes, StandaloneDeriving,
             TemplateHaskell, UndecidableInstances #-}
module Main where

import Prelude                     hiding (lookup)

import Control.DeepSeq             (NFData (..))
import Control.Monad               (replicateM_, void, when)
import Control.Monad.IO.Class      (liftIO)
import Control.Monad.Logger        (runNoLoggingT)
import Data.ByteString             (ByteString)
import Data.Char                   (chr)
import Data.Store                  (Store)
import Data.String                 (IsString)
import Data.String.Conv            (toS)
import Data.Text                   (Text)
import Database.Persist            as P
import Database.Persist.Postgresql
    ( ConnectionString
    , PostgresConf (..)
    , rawExecute
    , runMigration
    , runSqlPersistMPool
    , withPostgresqlPool
    )
import Database.Persist.TH
    (mkMigrate, mkPersist, persistLowerCase, share, sqlSettings)
import Database.Redis
    ( ConnectInfo (..)
    , PortID (..)
    , checkedConnect
    , defaultConnectInfo
    , flushall
    , runRedis
    )
import GHC.Generics                (Generic)
import Lens.Micro.Platform         ((&), (.~))
import System.Random               (mkStdGen, randomIO, randomRs)
import Text.Read                   (readMaybe)

import Criterion.Main              (defaultMainWith)
import Criterion.Main.Options      (defaultConfig)
import Criterion.Types             as Crit
-- import Database.Redis              as R


import Data.Lru3LCache
import Data.Lru3LCache.PgRedis



critCfg :: Crit.Config
critCfg = defaultConfig {reportFile  = Just "Bench-Lru3LCache.html" }


host :: IsString s => s
host = "localhost"
-- host = "gondoleto"

dbRecords, searchKeys :: Int
dbRecords  = 5000
searchKeys = 1000

pgConnectionStr :: ConnectionString
pgConnectionStr = "dbname=lru-test host=" <> host <> " user=vlatko password=pero port=5432"


share [mkPersist sqlSettings, mkMigrate "migrateAll"] [persistLowerCase|
Test
    str  String
    txt  Text
    bs   ByteString
    myb  Int Maybe
    int  Int
    deriving Show Read Generic
|]
instance Store  Test
instance NFData Test

instance NFData (PgRedCache Int Test) where rnf _ = ()


runMissTest :: Lru3LLevel -> Int -> Int -> Benchmarkable
runMissTest lvl nKeys rangeTo = do
  Crit.perRunEnv getEnv (uncurry (lookupTest lvl))
  where
    getEnv = do
      c <- mkTestCache pgConnectionStr host
      let keys = take nKeys $ randomRs (0,rangeTo) (mkStdGen 0)
      return (c, keys)

runSearchTest :: Bool -> Int -> Int -> Benchmarkable
runSearchTest clearL1 nKeys rangeTo = do
  Crit.perRunEnv getEnv (uncurry (lookupTest L1Level))
  where
    getEnv = do
      c <- mkTestCache pgConnectionStr host
      let keys = take nKeys $ randomRs (0,rangeTo) (mkStdGen 0)
      void $ lookupTest BothLevels c keys
      when clearL1 $ clearCache c
      return (c, keys)

main :: IO ()
main = do
  initDb pgConnectionStr dbRecords  -- drop and fill only once
  defaultMainWith critCfg [
    bgroup "1k rand lookup" [
        bench "L3 -> L1"    $ runMissTest   L1Level    dbRecords searchKeys
      , bench "L3 -> L2"    $ runMissTest   L2Level    dbRecords searchKeys
      , bench "L3 -> L1&L2" $ runMissTest   BothLevels dbRecords searchKeys
      , bench "L2 -> L1"    $ runSearchTest True       dbRecords searchKeys
      , bench "All in L1"   $ runSearchTest False      dbRecords searchKeys
      ]
    ]


lookupTest :: Lru3LClass l2 l3 Int Test
           => Lru3LLevel -> Lru3LCache l2 l3 Int Test -> [Int] -> IO [Maybe Test]
lookupTest lvl c' ks = let c = setFromL3InsertTo lvl c' in mapM (lookup c) ks


mkTestCache :: ConnectionString -> String -> IO (PgRedCache Int Test)
mkTestCache connStrPG redisHost = do
  l2  <- mkRedisCfg
  l3  <- mkPostgresCfg
  l3l <- pgRedInit l2 l3 2000
  void $ runRedis (connection l2) flushall   -- clear Redis DB on each call
  return $ l3l & evictToL2 .~ False
  where
    redConnInfo = defaultConnectInfo
      { connectHost           = redisHost
      , connectPort           = PortNumber 6379 -- Redis default port
      , connectAuth           = Nothing         -- No password
      , connectDatabase       = 0               -- SELECT database 0
      , connectMaxConnections = 50              -- Up to 50 connections
      , connectMaxIdleTime    = 30              -- Keep open for 30 seconds
      }
    mkRedisCfg = do
      conn <- checkedConnect redConnInfo
      return RedisCfg { uniqAppId  = "cachePrefix"
                      , cacheId    = "AppIdent"
                      , ttlSec     = 200
                      , connection = conn
                      , keyToBS    = toS . show
                      , valToBS    = toS . show
                      , valFromBS  = maybe (Left "error") Right . readMaybe . toS
                      }

    mkPostgresCfg = do
      let pc = PostgresConf connStrPG 1 1 10
      pgPool <- createPoolConfig pc
      return PostgresCfg { pool      = pgPool
                         , findByKey = P.get . intToKey
                         }

mkTestItem :: IO Test
mkTestItem = do
  int <- randomIO
  return Test{ testStr = mkRandStr 1024
             , testTxt = mkRandStr 1024
             , testBs  = mkRandStr 1024
             , testMyb = Nothing
             , testInt = int
             }
  where
    mkRandStr n = toS . map chr $ take n $ randomRs (0,255) (mkStdGen 0)

intToKey :: (PersistEntity record) => Int -> Key record
intToKey i = case keyFromValues [PersistInt64 $ fromIntegral i] of
  Right k -> k
  _       -> error ("bad key conversion" :: String)


initDb :: ConnectionString -> Int -> IO ()
initDb connStr n = do
  runNoLoggingT $ withPostgresqlPool connStr 10 $ \pool ->
    liftIO $ flip runSqlPersistMPool pool $ do
      rawExecute "drop table if exists \"test\";" []
      runMigration migrateAll
      replicateM_ n (P.insert =<< liftIO mkTestItem)
