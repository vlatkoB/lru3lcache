module PostgresSpec (main, spec) where

import           Prelude                       hiding (lookup)

import           Control.Monad                 (void)
import           Data.String                   (IsString)
import           Database.Persist.Postgresql   (ConnectionString)
import qualified Database.Redis                as R
import           Lens.Micro.Platform           ((^.))

import           Test.Hspec                    (Spec, describe, hspec, it, shouldReturn)


import           Data.Lru3LCache               (l2Config)
import           Data.Lru3LCache.Backend.Redis (RedisCfg (..))
import           Data.Lru3LCache.Test.Tests
import           PgRedisCache                  (insPgTests, mkPgCacheTest)


host :: IsString s => s
host = "localhost"

pgConnectionStr :: ConnectionString
pgConnectionStr = "dbname=lru-test host=" <> host <> " user=vlatko password=pero port=5432"


main :: IO ()
main = hspec spec

spec :: Spec
spec = do
  describe "NoL2: Cache 3 Levels" $ mapM_ runCacheTest [1,2,31,32,33,4,5,61,62,63]

  where
    runCacheTest n = it ("Test " <> show n) $ runTest n `shouldReturn` True


-- | Run specified test on Redis L2 cache with no L3 backend.
runTest :: Int -> IO Bool
runTest n = do
      c <- mkPgCacheTest pgConnectionStr host
      insPgTests c (2,20)
      void $ R.runRedis (connection $ c ^. l2Config) R.flushall
      go c n

  where
    go cache test = case test of
      1  -> test1   cache
      2  -> test2   cache
      31 -> test3_1 cache
      32 -> test3_2 cache
      33 -> test3_3 cache
      4  -> test4   cache
      5  -> test5   cache
      61 -> test6_1 cache
      62 -> test6_2 cache
      63 -> test6_3 cache
      _  -> return False
