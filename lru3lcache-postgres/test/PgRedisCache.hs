module PgRedisCache
  ( mkPgCacheTest
  , insPgTests
  )
where

import Prelude                     hiding (lookup)

import Database.Persist            as P hiding (insert)
import Database.Persist.Postgresql (ConnectionString, PostgresConf (..), rawExecute,
                                    runMigration)
import Database.Persist.Sql        (runSqlPool)
import Lens.Micro.Platform         ((&), (.~), (^.))
import UnliftIO                    (MonadUnliftIO)


import Data.Lru3LCache             (Lru3LCache, Lru3LLevel (..), fromL3InsertTo, l3Config)
import Data.Lru3LCache.PgRedis     (PgRedCache, PostgresCfg (..), pgRedInit)

import Common                      (Test (..), intToKey, migrateAll, mkRedisCfg)


-- | Create Lru3LCache of Postgres and Redis backends combination
mkPgCacheTest :: ConnectionString -> String -> IO (PgRedCache Int Test)
mkPgCacheTest connStrPG redisHost = do
  l2 <- mkRedisCfg redisHost
  l3 <- mkPostgresCfg
  c3l <- pgRedInit l2 l3 3
  pgRun l3 $ do
    rawExecute "drop table if exists \"test\";" []
    runMigration migrateAll
  return $ c3l & fromL3InsertTo .~ L2Level
  where
    mkPostgresCfg = do
      let pc = PostgresConf connStrPG 1 1 10
      pgPool <- createPoolConfig pc
      return PostgresCfg { pool      = pgPool
                         , findByKey = P.get . intToKey
                         }
    pgRun PostgresCfg{..} a = runSqlPool a pool

-- | Insert some data into Postgres table
insPgTests :: MonadUnliftIO m => Lru3LCache l2 PostgresCfg k v -> (Int,Int) -> m ()
insPgTests ((^. l3Config) -> PostgresCfg{..}) (from,to) = do
  mapM_ (runDB . P.insertUnique . Test) [from .. to]
  where
    runDB a = runSqlPool a pool
