{-# LANGUAGE DataKinds, DerivingStrategies, QuasiQuotes, StandaloneDeriving,
             TemplateHaskell, UndecidableInstances #-}
module Common
  ( mkRedisCfg
  , migrateAll
  , Test (..)
  , TestId

  , intToKey
  )
where

import           Prelude                    hiding (lookup)

import           Control.DeepSeq            (NFData)
import           Data.Aeson                 (FromJSON (..), ToJSON (..))
import           Data.Hashable              (Hashable)
import qualified Data.Store                 as S
import           Data.String.Conv           (toS)
import           Database.Persist
    (Key, PersistEntity, PersistValue (..), keyFromValues)
import           Database.Persist.TH
    (mkMigrate, mkPersist, persistLowerCase, share, sqlSettings)
import           Database.Redis             (ConnectInfo (..), PortID (..))
import qualified Database.Redis             as R
import           GHC.Generics               (Generic)
import           Text.Read                  (readMaybe)


import           Data.Lru3LCache.PgRedis    (RedisCfg (..))
import           Data.Lru3LCache.Test.Tests (Lru3LCacheTest (..))


-- | Datatype used in all tests. Fieldname is "testSome_int".
share [mkPersist sqlSettings, mkMigrate "migrateAll"] [persistLowerCase|
Test
    some_int  Int
    deriving Show Read Eq Ord Generic
|]
instance NFData   Test
instance Hashable Test
instance S.Store  Test
instance ToJSON   Test
instance FromJSON Test
instance Lru3LCacheTest Test where mkTestItem = Test


-- | Create L2Level 'Redis' cache
mkRedisCfg :: (Show k, Show v, Read v) => String -> IO (RedisCfg k v)
mkRedisCfg redisHost= do
  conn <- R.checkedConnect redConnInfo
  return RedisCfg { uniqAppId  = "cachePrefix"
                  , cacheId    = "AppIdent"
                  , ttlSec     = 200
                  , connection = conn
                  , keyToBS    = toS . show
                  , valToBS    = toS . show
                  , valFromBS  = maybe (Left "error") Right . readMaybe . toS
                  }
  where
    redConnInfo  = R.defaultConnectInfo
      { connectHost           = redisHost
      , connectPort           = PortNumber 6379 -- Redis default port
      , connectAuth           = Nothing         -- No password
      , connectDatabase       = 0               -- SELECT database 0
      , connectMaxConnections = 50              -- Up to 50 connections
      , connectMaxIdleTime    = 30              -- Keep open for 30 seconds
      , connectTimeout        = Nothing         -- Don't add timeout logic
      }

------------------------------------------------------------------------------------------
-- Helpers
------------------------------------------------------------------------------------------

-- | Convert 'Int' to 'Key'
intToKey :: (PersistEntity record) => Int -> Key record
intToKey i = case keyFromValues [PersistInt64 $ fromIntegral i] of
  Right k -> k
  _       -> error ("bad key conversion" :: String)
