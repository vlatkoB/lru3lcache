# lru3lcache

Lru3LCache is a last-recently-used cache with two levels, like a CPU.
L1 is smallest and fastest and resides in memory as a 'HashMap', L2 is slower and larger,
usualy on another machine and can be shared in HA environment with other peers.
Lastly, while L3 is only the data source, the last chance for finding data.


See more info in main Lru3LCache module
