-- | Module      : Data.Lru3LCache.Backend.Postgres
--   Description : Postgres backend for Lru3LCache
--   Copyright   : (c) Vlatko Bašić, 2018
--   License     : BSD
--   Maintainer  : vlatko.basic@gmail.com
--   Stability   : beta
--   Portability : portable
--
module Data.Lru3LCache.Backend.Postgres
  ( PostgresCfg (..)
  , PgNoL2Cache
  , pgGet
  )
where

import Database.Persist.Postgresql (ConnectionPool)
import Database.Persist.Sql        (SqlPersistT, runSqlPool)
import Lens.Micro.Platform         ((^.))


import Data.Lru3LCache             (Lru3LCache, l3Config)
import Data.Lru3LCache.Class       (Lru3LClass (..), NoL2 (..))



type PgNoL2Cache k v = Lru3LCache NoL2 PostgresCfg k v
instance Lru3LClass NoL2 PostgresCfg k v where lookupL3 = pgGet


-- | Configuration for Postgres backend based on 'Persist.Postgresql'
data PostgresCfg k v = PostgresCfg
  { pool      :: ConnectionPool                -- ^ Connection pool to use
  , findByKey :: k -> SqlPersistT IO (Maybe v) -- ^ Fetch a value by the key
  }

-- | Search for a key in PG database
pgGet :: Lru3LCache l2 PostgresCfg k v -> k -> IO (Maybe v)
pgGet ((^. l3Config) -> PostgresCfg{..}) k = runSqlPool (findByKey k) pool
