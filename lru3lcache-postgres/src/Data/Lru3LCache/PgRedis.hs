-- | Module      : Data.Lru3LCache.PgRedis
--   Description : Redis backend for Lru3LCache
--   Copyright   : (c) Vlatko Bašić, 2018
--   License     : BSD
--   Maintainer  : vlatko.basic@gmail.com
--   Stability   : beta
--   Portability : portable
--
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Data.Lru3LCache.PgRedis
  ( PgRedCache
  , RedisCfg (..)
  , PostgresCfg (..)
  , pgRedInit
  )
where

import Data.Lru3LCache                  (Lru3LCache, Lru3LClass (..), initialize)
import Data.Lru3LCache.Backend.Postgres (PostgresCfg (..), pgGet)
import Data.Lru3LCache.Backend.Redis    (RedisCfg (..), redisGetKeyTick, redisIncrTick,
                                         redisLookup, redisRemove, redisSetTick,
                                         redisStore)


-- | Convenient type for Redis-Postgres backend combo
type PgRedCache k v = Lru3LCache RedisCfg PostgresCfg k v

instance Lru3LClass RedisCfg PostgresCfg k v where
  lookupL2     = redisLookup
  storeL2      = redisStore
  removeL2     = redisRemove
  incrTickL2   = redisIncrTick
  getKeyTickL2 = redisGetKeyTick
  setTickL2    = redisSetTick
  lookupL3     = pgGet


-- | Initialize PgRedis
pgRedInit :: RedisCfg k v -> PostgresCfg k v -> Int -> IO (PgRedCache k v)
pgRedInit = initialize
