-- | Module      : Data.Lru3LCache.ElasticSearch
--   Description : Redis backend for Lru3LCache
--   Copyright   : (c) Vlatko Bašić, 2018
--   License     : BSD
--   Maintainer  : vlatko.basic@gmail.com
--   Stability   : beta
--   Portability : portable
--
module Data.Lru3LCache.Backend.ElasticSearch
  ( ElasticCfg (..)
  , EsNoL2Cache
  , esGet
  )
where

import Lens.Micro.Platform   ((^.))

import Data.Lru3LCache       (Lru3LCache, l3Config)
import Data.Lru3LCache.Class (Lru3LClass (..), NoL2 (..))



type EsNoL2Cache k v = Lru3LCache NoL2 ElasticCfg k v
instance Lru3LClass NoL2 ElasticCfg k v where lookupL3 = esGet


-- | Configuration for ElasticSearch backend
newtype ElasticCfg k v = ElasticCfg {search :: k -> IO (Maybe v)}

-- | Search for a key in PG database
esGet :: Lru3LCache l2 ElasticCfg k v -> k -> IO (Maybe v)
esGet ((^. l3Config) -> ElasticCfg{..}) = search
