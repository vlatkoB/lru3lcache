-- | Module      : Data.Lru3LCache.EsRedis
--   Description : 3 level caching
--   Copyright   : (c) Vlatko Bašić, 2018
--   License     : BSD
--   Maintainer  : vlatko.basic@gmail.com
--   Stability   : beta
--   Portability : portable
--
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Data.Lru3LCache.EsRedis
  ( EsRedCache
  , RedisCfg (..)
  , ElasticCfg (..)
  , esRedInit
  )
where

import Data.Lru3LCache                       (Lru3LCache, Lru3LClass (..), initialize)
import Data.Lru3LCache.Backend.ElasticSearch (ElasticCfg (..), esGet)
import Data.Lru3LCache.Backend.Redis         (RedisCfg (..), redisGetKeyTick,
                                              redisIncrTick, redisLookup, redisRemove,
                                              redisSetTick, redisStore)


-- | Convenient type for Redis-ElasticSearch backend combo
type EsRedCache k v = Lru3LCache RedisCfg ElasticCfg k v

instance Lru3LClass RedisCfg ElasticCfg k v where
  lookupL2     = redisLookup
  storeL2      = redisStore
  removeL2     = redisRemove
  incrTickL2   = redisIncrTick
  getKeyTickL2 = redisGetKeyTick
  setTickL2    = redisSetTick
  lookupL3     = esGet


-- | Initialize EsRedis
esRedInit :: RedisCfg k v -> ElasticCfg k v -> Int -> IO (EsRedCache k v)
esRedInit = initialize
