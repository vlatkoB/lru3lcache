module Common
  ( mkRedisCfg
  , Test (..)

  , tshow
  , headMay
  )
where

import           Prelude                       hiding (lookup)

import           Control.DeepSeq               (NFData)
import           Data.Aeson                    (FromJSON (..), ToJSON (..))
import           Data.Hashable                 (Hashable)
import qualified Data.Store                    as S
import           Data.String.Conv              (toS)
import           Data.Text                     (Text, pack)
import           Database.Redis                (ConnectInfo (..), PortID (..))
import qualified Database.Redis                as R
import           GHC.Generics                  (Generic)
import           Text.Read                     (readMaybe)

import           Data.Lru3LCache.Backend.Redis (RedisCfg (..))
import           Data.Lru3LCache.Test.Tests    (Lru3LCacheTest (..))


data Test = Test {testSome_int :: Int}
  deriving (Show,Read,Eq,Ord,Generic)
instance NFData   Test
instance Hashable Test
instance S.Store  Test
instance ToJSON   Test
instance FromJSON Test
instance Lru3LCacheTest Test where mkTestItem = Test

-- | Create L2Level 'Redis' cache
mkRedisCfg :: (Show k, Show v, Read v) => String -> IO (RedisCfg k v)
mkRedisCfg redisHost= do
  conn <- R.checkedConnect redConnInfo
  return RedisCfg { uniqAppId  = "cachePrefix"
                  , cacheId    = "AppIdent"
                  , ttlSec     = 200
                  , connection = conn
                  , keyToBS    = toS . show
                  , valToBS    = toS . show
                  , valFromBS  = maybe (Left "error") Right . readMaybe . toS
                  }
  where
    redConnInfo  = R.defaultConnectInfo
      { connectHost           = redisHost
      , connectPort           = PortNumber 6379 -- Redis default port
      , connectAuth           = Nothing         -- No password
      , connectDatabase       = 0               -- SELECT database 0
      , connectMaxConnections = 50              -- Up to 50 connections
      , connectMaxIdleTime    = 30              -- Keep open for 30 seconds
      , connectTimeout        = Nothing         -- Don't add timeout logic
      }

------------------------------------------------------------------------------------------
-- Helpers
------------------------------------------------------------------------------------------

-- | Show var as 'Text'
tshow :: Show a => a -> Text
tshow = pack . show

-- | Safely get the 'head' of the list
headMay :: [a] -> Maybe a
headMay (x : _) = Just x
headMay _ = Nothing
