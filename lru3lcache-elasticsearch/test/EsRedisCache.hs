module EsRedisCache
  ( mkEsCacheTest
  , insEsTests
  )
where

import Prelude                 hiding (lookup)

import Control.Monad           (void)
import Data.Aeson              (ToJSON (..), object, (.=))
import Data.Text               (Text)
import Database.V5.Bloodhound  (DocId (..), IndexName, IndexSettings (..),
                                MappingName (..), Query (..), ReplicaCount (..), Server,
                                ShardCount (..), Term (..), createIndex,
                                defaultIndexDocumentSettings, deleteIndex, hitSource,
                                hits, indexDocument, mkSearch, parseEsResponse,
                                putMapping, refreshIndex, searchByType, searchHits,
                                withBH)
import Lens.Micro.Platform     ((^.))
import Network.HTTP.Client     (defaultManagerSettings)

import Data.Lru3LCache         (Lru3LCache, l3Config)
import Data.Lru3LCache.EsRedis (ElasticCfg (..), EsRedCache, esRedInit)

import Common                  (Test (..), headMay, mkRedisCfg, tshow)


-- | Mapping for Test data
data SpecMapping  = SpecMapping deriving (Eq, Show)
instance ToJSON SpecMapping where
  toJSON SpecMapping =
    object [ "properties" .=
      object [ fieldName .= object ["type" .= ("integer" :: Text)] ]
           ]

-- | Name of the field used in mapping and queries. Must be the same as 'Persistent'
--   field name from 'Common'
fieldName :: Text
fieldName = "testSome_int"

-- | Name of the mapping used throughout tests
mapping :: MappingName
mapping = MappingName "doc"


-- | Create Lru3LCache of ElasticSearch and Redis backends combination
mkEsCacheTest :: Server -> IndexName -> String -> IO (EsRedCache Int Test)
mkEsCacheTest esSrv esIdx redisHost = do
  l2 <- mkRedisCfg redisHost
  l3 <- mkL3Config
  esRedInit l2 l3 3
  where
    mkL3Config = return ElasticCfg { search = l3Search }
    l3Search :: Int -> IO (Maybe Test)
    l3Search i = withBH defaultManagerSettings esSrv $ do
        let qry  = TermQuery (Term fieldName $ tshow i) Nothing
            srch = mkSearch (Just qry) Nothing
        either (const Nothing) ((hitSource =<<) . headMay . hits . searchHits)
           <$> (parseEsResponse =<< searchByType esIdx mapping srch)

-- | Insert some data into ElasticSearch
insEsTests :: Server -> IndexName -> Lru3LCache l2 ElasticCfg k v -> IO ()
insEsTests srv idx ((^. l3Config) -> ElasticCfg{}) = runBH' $ do
  void $ deleteIndex idx
  void $ createIndex (IndexSettings (ShardCount 1) (ReplicaCount 0)) idx
  void $ putMapping idx mapping SpecMapping
  mapM_ insItem [2::Int .. 20]
  void $ refreshIndex idx

  where
    insItem n = indexDocument idx mapping defaultIndexDocumentSettings (Test n) (DocId $ tshow n)
    runBH' = withBH defaultManagerSettings srv
