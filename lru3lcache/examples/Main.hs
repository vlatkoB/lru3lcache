module Main where

import           Prelude                hiding (lookup)

import           Control.Monad          (void)
import           Data.ByteString        (ByteString)
-- import           Data.Hashable          (Hashable)
-- import qualified Data.HashPSQ           as HQ
-- import           Data.IORef             (readIORef)
-- import qualified Data.LruCache.Internal as CLI
import           Data.Monoid            ((<>))
-- import           Data.String.Conv       (toS)
import           Data.Text              (Text)
import           Data.Time.Clock        (UTCTime)
import           Database.Persist       as P hiding (insert)
import           Options.Applicative    (execParser, fullDesc, header, help, helper, info,
                                         long, metavar, progDesc, short, showDefault,
                                         strOption, value, (<**>))


-- import           Data.Lru3LCache
import           PgRedis  (pgCacheMain)
import           EsRedis  (esCacheMain)
-- import           Data.Lru3LCache.EsRedis
-- import           Data.LruL3Cache.Test

-- import Import.Debug


------------------------------------------------------------------------------------------
-- Main entry point
------------------------------------------------------------------------------------------


-- | Command line arguments
data Args =
  Args { argFileName :: FilePath  -- ^ file path of Xliff file
       , argTest     :: Text
       } deriving (Show)

type ParseVal = (UTCTime, ByteString)

-- | Main app entry point
main :: IO ()
main = do
  Args{..} <- execParser pInfo
  void $ pgCacheMain False
  void $ esCacheMain False
  return ()

  where
    pInfo = info (args <**> helper)
                 (fullDesc <> progDesc "Xliff parser tester"
                           <> header   "Xliff parser tester")
    args = Args <$> strOption (long "xlf"  <> short 'f'
                            <> value "data/xliff20-sample-small.xlf" <> showDefault
                            <> help "Filename to parse")
                <*> strOption ( long "test" <> short 't'
                             <> help "Test to execute"
                             <> value "none" <> showDefault
                             <> metavar "STRING" )

-- dbgHQ :: (Show v, Show k, Ord k, Hashable k) => Lru3LCache l2 l3 k v -> Text -> IO ()
-- dbgHQ cache msg = do
--   (hq::[(k, CLI.Priority, v)]) <- HQ.toList . CLI.lruQueue <$> readIORef (lruHandle cache)
--   putStrLn ""
--   if null hq
--     then putStrLn $ toS msg <> " - Empty"
--     else do
--       putStrLn $ toS msg <> ": "
--       mapM_ (\x -> putStrLn $ "  - " <> show x) hq
--   putStrLn ""


intToKey :: (PersistEntity record) => Int -> Key record
intToKey i = case keyFromValues [PersistInt64 $ fromIntegral i] of
  Right k -> k
  _       -> error ("bad key conversion" :: String)

prompt :: String -> IO ()
prompt msg = do
  putStrLn $ msg <> ": "
  void getLine
