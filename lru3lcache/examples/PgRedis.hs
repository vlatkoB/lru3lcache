{-# LANGUAGE QuasiQuotes, TemplateHaskell #-}
module PgRedis where

import           Prelude                     hiding (lookup)

import           Control.Monad               (void, when)
import           Data.ByteString             (ByteString)
import           Data.ByteString.Char8       (pack, unpack)
import           Data.Monoid                 ((<>))
import           Data.Store                  (Store)
import           Data.Text                   (Text)
import           Database.Persist            as P hiding (insert)
import           Database.Persist.Postgresql (ConnectionString, PostgresConf (..),
                                              rawExecute, runMigration)
import           Database.Persist.TH         (mkMigrate, mkPersist, persistLowerCase,
                                              share, sqlSettings)
import           Database.Redis              (ConnectInfo (..), PortID (..))
import qualified Database.Redis              as R
import           GHC.Generics                (Generic)
import           Lens.Micro.Platform         ((^.))
import           Text.Read                   (readMaybe)

-- import           Data.String.Conv            (toS)
-- import           UnliftIO                    (MonadUnliftIO)
-- import           Control.DeepSeq             (NFData)
-- import           Data.Hashable               (Hashable)
-- import qualified Data.HashPSQ                as HQ
-- import           Data.IORef                  (readIORef)
-- import qualified Data.LruCache.Internal      as CLI
-- import qualified Data.LruCache.IO            as CIO

import           Data.Lru3LCache
import           Data.Lru3LCache.PgRedis


------------------------------------------------------------------------------------------
-- Main entry point
------------------------------------------------------------------------------------------

share [mkPersist sqlSettings, mkMigrate "migrateAll"] [persistLowerCase|
Test
    str  String
    txt  Text
    bs   ByteString
    myb  Int Maybe
    int  Int
    deriving Show Read Generic
|]
instance Store  Test
-- instance NFData Test

-- dbgHQ :: (Show v, Show k, Ord k, Hashable k) => Lru3LCache l2 l3 k v -> Text -> IO ()
-- dbgHQ cache msg = do
--   (hq::[(k, CLI.Priority, v)]) <- HQ.toList . CLI.lruQueue <$> readIORef (lruHandle cache)
--   putStrLn ""
--   if null hq
--     then putStrLn $ toS msg <> " - Empty"
--     else do
--       putStrLn $ toS msg <> ": "
--       mapM_ (\x -> putStrLn $ "  - " <> show x) hq
--   putStrLn ""

pgCacheMain :: Bool -> IO ()
pgCacheMain flushRedis = do
  cache <- mkPgCacheTest pgConnStr "localhost"
  when flushRedis $ void $ R.runRedis (connection $ cache ^. l2Config) R.flushall
  prompt "Start 1"
  -- insTests cache
  c1 <- lookup cache 1
  prompt $ "C1: " <> show c1
  -- dbgHQ cache "Cache1"
  c4 <- lookup cache 4
  prompt $ "C4: " <> show c4
  -- dbgHQ cache "Cache4"
  c12 <- lookup cache 1
  prompt $ "C12: " <> show c12
  -- dbgHQ cache "Cache12"
  -- _cs1 <- mapM (\n -> lookup cache n) [1::Int ..12]
  -- _cs2 <- mapM (\n -> lookup cache n) [1::Int ..12]
  -- mapM_ putStrLn cs
  c41 <- lookup cache 6
  prompt $ "C41: " <> show c41
  c42 <- lookup cache 6
  prompt $ "C42: " <> show c42
  c43 <- lookup cache 6
  prompt $ "C43: " <> show c43
  -- dbgHQ cache "Cend"
  return ()
  where
    pgConnStr = "dbname=tram host=127.0.0.1 user=vlatko password=pero port=5432"


mkPgCacheTest :: ConnectionString -> String -> IO (PgRedCache Int Test)
mkPgCacheTest connStrPG redisHost = do
  putStrLn $ "Connection string: " <> show connStrPG
  l2 <- mkL2Config
  l3 <- mkL3Lonfig
  c3l <- pgRedInit l2 l3 3
  pgRun l3 $ do
    rawExecute "drop table if exists \"test\";" []
    runMigration migrateAll
  insTests c3l
  return c3l
  where
    redConnInfo = R.defaultConnectInfo
      { connectHost           = redisHost
      , connectPort           = PortNumber 6379 -- Redis default port
      , connectAuth           = Nothing         -- No password
      , connectDatabase       = 0               -- SELECT database 0
      , connectMaxConnections = 50              -- Up to 50 connections
      , connectMaxIdleTime    = 30              -- Keep open for 30 seconds
      , connectTimeout        = Nothing         -- Don't add timeout logic
      -- , connectTLSParams      = Nothing         -- Do not use TLS
      }
    mkL2Config = do
      conn <- R.checkedConnect redConnInfo
      return RedisCfg { uniqAppId  = "cachePrefix"
                      , cacheId    = "AppIdent"
                      , ttlSec     = 8 * 60 * 60 -- 8 hours
                      , connection = conn
                      , keyToBS    = pack . show
                      , valToBS    = pack . show
                      , valFromBS  = maybe (Left "error") Right . readMaybe . unpack
                      }

    mkL3Lonfig = do
      let pc = PostgresConf connStrPG 10
      pgPool <- createPoolConfig pc
      return PostgresCfg { config    = pc
                         , pool    = pgPool
                         , findByKey = P.get . intToKey
                         }
    pgRun PostgresCfg{..} a = runPool config a pool
    insTests ((^. l3Config) -> PostgresCfg{..}) = do
      return ()
      mapM_ (runDB . P.insertUnique . mkTestItem) [2::Int .. 20]
      where
        runDB a = runPool config a pool
        mkTestItem :: Int -> Test
        mkTestItem n = Test ("Cache Test" <> show n) "TXT" "BS" Nothing 111


intToKey :: (PersistEntity record) => Int -> Key record
intToKey i = case keyFromValues [PersistInt64 $ fromIntegral i] of
  Right k -> k
  _       -> error ("bad key conversion" :: String)

prompt :: String -> IO ()
prompt msg = do
  putStrLn $ "Press Enter:   " <> msg <> ": "
  void getLine
