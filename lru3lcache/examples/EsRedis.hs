{-# LANGUAGE UndecidableInstances, ScopedTypeVariables, TemplateHaskell, TypeApplications #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DerivingStrategies #-}

{-# OPTIONS_GHC -fno-warn-orphans #-}
module EsRedis where

import           Prelude                 hiding (lookup)

import           Control.Monad           (void, when)
import           Control.Monad.IO.Class  (MonadIO, liftIO)
import           Data.Aeson              (FromJSON (..), decode, defaultOptions,
                                          eitherDecode, encode, genericParseJSON,
                                          genericToJSON, object, (.=))
import           Data.ByteString.Lazy    (ByteString)
import           Data.Hashable           (Hashable)
-- import qualified Data.HashPSQ            as HQ
-- import           Data.IORef              (readIORef)
import           Data.List.NonEmpty      (NonEmpty (..))
-- import qualified Data.LruCache.Internal  as CLI
import           Data.Maybe              (mapMaybe)
import           Data.Monoid             ((<>))
import qualified Data.Store              as S
import           Data.String.Conv        (toS)
import           Data.Text               (Text, pack)
import           Data.Time.Calendar      (Day (..))
import           Data.Time.Clock         (UTCTime (..), secondsToDiffTime)
import           Database.Redis          (ConnectInfo (..), PortID (..))
import qualified Database.Redis          as R
import           Database.V5.Bloodhound
import           GHC.Generics            (Generic)
import           Lens.Micro.Platform     ((^.))
import           Network.HTTP.Client     (Response (..), defaultManagerSettings)
import           Network.HTTP.Types      (Status (..))
import           Text.Read               (readMaybe)
import           TH.Derive ()

-- import qualified Data.LruCache.IO        as CIO
-- import           Data.Monoid                 ((<>))
-- import           Data.ByteString             (ByteString)
-- import           Text.Read               (readMaybe)
-- import Data.Functor.Const
-- import           Control.Monad           (void)


import           Data.Lru3LCache
import           Data.Lru3LCache.EsRedis

-- import Import.Debug


deriving stock instance Generic LatLon
deriving anyclass instance S.Store LatLon
-- $($(derive [d| instance Deriving (S.Store LatLon) |]))

------------------------------------------------------------------------------------------
-- Main entry point
------------------------------------------------------------------------------------------
mapping :: MappingName
mapping = MappingName "doc"

-- dbgHQ :: (Show v, Show k, Ord k, Hashable k) => Lru3LCache l2 l3 k v -> Text -> IO ()
-- dbgHQ cache msg = do
--   (hq::[(k, CLI.Priority, v)]) <- HQ.toList . CLI.lruQueue <$> readIORef (lruHandle cache)
--   putStrLn ""
--   if null hq
--     then putStrLn $ toS msg <> " - Empty"
--     else do
--       putStrLn $ toS msg <> ": "
--       mapM_ (\x -> putStrLn $ "  - " <> show x) hq
--   putStrLn ""

esCacheMain :: Bool -> IO ()
esCacheMain flushRedis = do
  cache <- mkEsCacheTest testServer testIndex  "localhost"
  when flushRedis $ void $ R.runRedis (connection $ cache ^. l2Config) R.flushall
  insTests cache

  -- prompt "Start 1"
  c1 <- lookup cache (mkTestSearch $ mkUsr 1)
  prompt $ "C1: " <> show c1
  -- print c1
  -- dbgHQ cache "Cache1"
  c4 <- lookup cache (mkTestSearch $ mkUsr 4)
  prompt $ "C4: " <> show c4
  -- dbgHQ cache "Cache4"
  c12 <- lookup cache (mkTestSearch $ mkUsr 12)
  prompt $ "C12: " <> show c12
  -- dbgHQ cache "Cache12"
  _cs2 <- mapM (lookup cache . mkTestSearch . mkUsr) [1::Int ..12]
  mapM_ print _cs2
  c41 <- lookup cache (mkTestSearch $ mkUsr 41)
  prompt $ "C41: " <> show c41
  c42 <- lookup cache (mkTestSearch $ mkUsr 42)
  prompt $ "C42: " <> show c42
  c43 <- lookup cache (mkTestSearch $ mkUsr 43)
  prompt $ "C43: " <> show c43
  -- dbgHQ cache "Cend"
  return ()
  where
    mkUsr :: Int -> Text
    mkUsr n = "user" <> tshow n

insTests :: Lru3LCache l2 ElasticCfg k v -> IO ()
insTests ((^. l3Config) -> ElasticCfg{..}) = runES $ do
  void $ deleteIndex testIndex
  void $ createIndex (IndexSettings (ShardCount 1) (ReplicaCount 0)) testIndex
  void $ putMapping testIndex mapping TweetMapping
  mapM_ insItem [2::Int .. 20]
  void $ refreshIndex testIndex

  where
    insItem n = indexDocument testIndex mapping defaultIndexDocumentSettings (mkItem n) (DocId $ tshow n)
    runES = withBH defaultManagerSettings testServer
    mkItem :: Int -> Tweet
    mkItem n = mkTweet ("user" <> tshow n) "Hi" (100 + n) Nothing

tshow :: Show a => a -> Text
tshow = pack . show


showResp :: MonadIO m => String -> Response ByteString -> m ()
showResp msg r = do
  let l = case statusCode $ responseStatus r of
           200 -> "OK  " <> show r
           201 -> "CR"
           404 -> "Not found"
           400 -> case eitherDecode (responseBody r) of
             Left e            -> "Error: " <> show e
             Right EsError{..} -> show r -- unpack errorMessage
           _   -> show r
  liftIO $ putStrLn $ msg <> ":  " <> l

mkEsCacheTest :: Server -> IndexName -> String -> IO (EsRedCache EsQuery [Tweet])
mkEsCacheTest esSrv esIdx redisHost = do
  putStrLn $ "Es server: " <> show esSrv <> "  Index: " <> show esIdx
  l2 <- mkL2Config
  l3 <- mkL3Config
  esRedInit l2 l3 3
  where
    redConnInfo = R.defaultConnectInfo
      { connectHost           = redisHost
      , connectPort           = PortNumber 6379 -- Redis default port
      , connectAuth           = Nothing         -- No password
      , connectDatabase       = 0               -- SELECT database 0
      , connectMaxConnections = 50              -- Up to 50 connections
      , connectMaxIdleTime    = 30              -- Keep open for 30 seconds
      , connectTimeout        = Nothing         -- Don't add timeout logic
      -- , connectTLSParams      = Nothing         -- Do not use TLS
      }
    mkL2Config = do
      conn <- R.checkedConnect redConnInfo
      return RedisCfg { uniqAppId  = "cachePrefix"
                      , cacheId    = "AppIdent"
                      , ttlSec     = 200
                      , connection = conn
                      , keyToBS    = toS . show
                      , valToBS    = toS . show
                      , valFromBS  = maybe (Left "error") Right . readMaybe . toS
                      }

    mkL3Config = return ElasticCfg { search = l3Search }
    l3Search :: EsQuery -> IO (Maybe [Tweet])
    l3Search (EsQuery esQry) = case decode esQry of
      Nothing           -> return Nothing
      Just (qry, boost) -> do
        withBH defaultManagerSettings esSrv $ Just .
          either (const [])
                 (mapMaybe hitSource . hits . searchHits)
             <$> (parseEsResponse =<< searchByType esIdx mapping (mkSearch qry boost))


prompt :: String -> IO ()
prompt msg = do
  putStrLn $ msg <> ": "
  void getLine

data Tweet = Tweet
  { user     :: Text
  , postDate :: UTCTime
  , message  :: Text
  , age      :: Int
  , location :: LatLon
  , extra    :: Maybe Text
  } deriving (Eq, Generic, Show, Read)
instance ToJSON   Tweet where toJSON    = genericToJSON defaultOptions
instance FromJSON Tweet where parseJSON = genericParseJSON defaultOptions
instance S.Store  Tweet
deriving stock instance Read LatLon


mkTweet :: Text -> Text -> Int -> Maybe Text -> Tweet
mkTweet u m a e = Tweet
  { user     = u
  , postDate = UTCTime (ModifiedJulianDay 55000) (secondsToDiffTime 10)
  , message  = m
  , age      = a
  , location = LatLon {lat = 40.12, lon = -71.3}
  , extra    = e
  }






testServer :: Server
testServer = Server "http://localhost:9200"
testIndex :: IndexName
testIndex = IndexName "es_redis"

newtype EsQuery = EsQuery ByteString
  deriving (Show, Eq, Ord, Generic, S.Store, Hashable, Read)
-- deriving instance Read Query
-- deriving instance Read Term
-- deriving instance Read Boost
-- deriving instance Read MatchQuery
-- deriving instance Read Filter
-- deriving instance Read MultiMatchQuery
-- deriving instance Read QueryString
-- deriving instance Read BoolQuery
-- deriving instance Read BoostingQuery

mkTestSearch :: Text -> EsQuery
mkTestSearch usr = do
  let boost = (Nothing :: Maybe Boost)
      query1 = TermQuery (Term "user" usr) boost
      query2  = TermsQuery "user" (usr :| [])
  EsQuery $ encode (query2, boost)

ss :: ByteString
ss = "[{\"term\":{\"user\":{\"value\":\"aqa\"}}},null]"
f2 :: Either String (Query, Maybe Boost)
f2 = eitherDecode ss

data TweetMapping  = TweetMapping deriving stock (Eq, Show)
instance ToJSON TweetMapping where
  toJSON TweetMapping =
    object [ "properties" .=
      object [ "location" .= object [ "type"      .= ("geo_point" :: Text)]
             , "user"     .= object [ "type"      .= ("text" :: Text)
                                    , "fielddata" .= True
                                    ]
               -- Serializing the date as a date is breaking other tests, mysteriously.
               -- , "postDate" .= object [ "type"   .= ("date" :: Text)
               --                        , "format" .= ("YYYY-MM-dd'T'HH:mm:ss.SSSZZ" :: Text)]
               , "message"  .= object ["type" .= ("text"    :: Text)]
               , "age"      .= object ["type" .= ("integer" :: Text)]
               , "extra"    .= object ["type" .= ("keyword" :: Text)]
               , "my_all"   .= object ["type" .= ("text"    :: Text)]
               ]]
