-- | Module      : Lru3LCache
--   Description : 3 level caching
--   Copyright   : (c) Vlatko Bašić, 2018
--   License     : BSD
--   Maintainer  : vlatko.basic@gmail.com
--   Stability   : beta
--   Portability : portable
--
{-# LANGUAGE TemplateHaskell #-}
{-# OPTIONS_HADDOCK hide #-}
module Data.Lru3LCache.Type
  ( Lru3LCache
  , lruHandle
  , lruMap
  , l2Config
  , l3Config
  , evictToL2
  , insertToL2
  , fromL3InsertTo

  -- * Store/remove level type
  , Lru3LLevel (..)

  , initialize
  )
where

import Data.HashPSQ           (HashPSQ)
import Data.IORef             (IORef, readIORef)
import Data.Kind              (Type)
import Data.LruCache.Internal (LruCache (..), Priority)
import Data.LruCache.IO       (LruHandle (..), newLruHandle)
import Data.Typeable          (Typeable)
import GHC.Generics           (Generic)
import Lens.Micro.Platform    (makeLenses)

-- | Specify cache level where to insert an item when found in __L3__
--   or which level(s) to remove from
data Lru3LLevel = L1Level    -- ^ store//remove only to//from L1
                | L2Level    -- ^ store//remove only to//from L2
                | L3Level    -- ^ store//remove only to//from L2
                | BothLevels -- ^ store//remove to//from both L1 & L2
  deriving (Show, Eq, Ord, Generic, Typeable)

-- | Structure holding __L2__ & __L3__ level configs and settings that affect the behaviour
data Lru3LCache (l2 :: Type -> Type -> Type) (l3 :: Type -> Type -> Type) k v = Lru3LCache
  { internalLruHandle :: LruHandle k v
  , _l2Config         :: l2 k v
  , _l3Config         :: l3 k v
  , _evictToL2        :: Bool
  , _insertToL2       :: Bool
  , _fromL3InsertTo   :: Lru3LLevel
  }
makeLenses ''Lru3LCache

-- | Get internalLruHandle for the underlying lruCache
lruHandle :: Lru3LCache l2 l3 k v -> IORef (LruCache k v)
lruHandle c = let LruHandle ref = internalLruHandle c in ref

-- | Get HashPSQ map driving the cache
lruMap :: Lru3LCache l2 l3 k v -> IO (HashPSQ k Priority v)
lruMap = fmap lruQueue . readIORef . lruHandle


-- | Initialize Lru3LCache config with __L2__ and __L3__ backends and specify __L1__ size
initialize :: l2 k v -> l3 k v -> Int -> IO (Lru3LCache l2 l3 k v)
initialize  l2 l3 n = do
  lru <- newLruHandle n
  return Lru3LCache { internalLruHandle = lru
                    , _l2Config         = l2
                    , _l3Config         = l3
                    , _fromL3InsertTo   = L2Level
                    , _evictToL2        = True
                    , _insertToL2       = False
                    }
