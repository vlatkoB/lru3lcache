-- | Module      : Data.Lru3LCache.Class
--   Description : Class for combining L2 & L3 backends
--   Copyright   : (c) Vlatko Bašić, 2018
--   License     : BSD
--   Maintainer  : vlatko.basic@gmail.com
--   Stability   : beta
--   Portability : portable
--
module Data.Lru3LCache.Class
  ( -- * Lru3LClass
   Lru3LClass (..)

  , NoL2 (..)
  , noL2LookupL2
  , noL2StoreL2
  , noL2RemoveL2
  , noL2IncrTickL2
  , noL2SetTickL2
  , noL2GetKeyTickL2

  , NoL3 (..)
  , noL3lookupL3
  )
where

import Data.Hashable        (Hashable)
import Data.Int             (Int64)

import Data.Lru3LCache.Type


-- | Convenient type for no L2 backend
data NoL2 k v = NoL2
data NoL3 k v = NoL3

-- | Class for implementing __L2__/__L3__ backend combination
class Lru3LClass l2 l3 k v where
  -- | Lookup for a key in __L2__ level
  lookupL2 :: Lru3LCache l2 l3 k v -> Int64 -> k -> IO (Maybe (v,Int64))
  lookupL2 = noL2LookupL2

  -- | Store the key/value to __L2__ level
  storeL2 :: Lru3LCache l2 l3 k v -> Int64 -> k -> v -> IO (Maybe v)
  storeL2 = noL2StoreL2

  -- | Remove key from __L2__ level
  removeL2 :: Hashable k => Lru3LCache l2 l3 k v -> k -> IO Int
  removeL2 = noL2RemoveL2

  -- | Increment tick value for the key in __L2__ level
  incrTickL2 :: Lru3LCache l2 l3 k v -> k -> IO ()
  incrTickL2 = noL2IncrTickL2

  -- | Set the tick value for the key in __L2__ level
  setTickL2 :: Lru3LCache l2 l3 k v -> Int64 -> k -> IO ()
  setTickL2 = noL2SetTickL2

  -- | Get a tick value for the key from __L2__ level
  getKeyTickL2 :: Hashable k => Lru3LCache l2 l3 k v -> k -> IO (Maybe Integer)
  getKeyTickL2 = noL2GetKeyTickL2

  -- | Lookup for a key in __L3__ level
  lookupL3 :: Lru3LCache l2 l3 k v -> k -> IO (Maybe v)
  -- lookupL3  _ _ = return Nothing


------------------------------------------------------------------------------------------
-- NoL2 functions
------------------------------------------------------------------------------------------
-- | No-op functions
noL2LookupL2 :: Lru3LCache l2 l3 k v -> Int64 -> k -> IO (Maybe (v,Int64))
noL2LookupL2 _ _ _ = return Nothing
{-# INLINE noL2LookupL2 #-}

noL2StoreL2 :: Monad m => p1 -> p2 -> p3 -> p4 -> m (Maybe a)
noL2StoreL2 _ _ _ _ = return Nothing
{-# INLINE noL2StoreL2 #-}

noL2RemoveL2 :: (Monad m, Num a) => p1 -> p2 -> m a
noL2RemoveL2 _ _ = return 0
{-# INLINE noL2RemoveL2 #-}

noL2IncrTickL2 :: Monad m => p1 -> p2 -> m ()
noL2IncrTickL2 _ _ = return ()
{-# INLINE noL2IncrTickL2 #-}

noL2SetTickL2 :: Monad m => p1 -> p2 -> p3 -> m ()
noL2SetTickL2 _ _ _ = return ()
{-# INLINE noL2SetTickL2 #-}

noL2GetKeyTickL2 :: Monad m => p1 -> p2 -> m (Maybe a)
noL2GetKeyTickL2 _ _ = return Nothing
{-# INLINE noL2GetKeyTickL2 #-}

------------------------------------------------------------------------------------------
-- NoL2 functions
------------------------------------------------------------------------------------------
noL3lookupL3 :: Monad m => p1 -> p2 -> m (Maybe a)
noL3lookupL3  _ _ = return Nothing
