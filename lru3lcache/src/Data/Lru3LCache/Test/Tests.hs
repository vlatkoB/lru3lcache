{-# LANGUAGE RankNTypes #-}
module Data.Lru3LCache.Test.Tests where

import Prelude         hiding (lookup)

import Control.Error   (isNothing)
import Control.Monad   (void)
import Data.Store      (Store)
import Data.Lru3LCache (Lru3LCache, Lru3LClass (..), Lru3LLevel (..), getCacheTick,
                        getKeyTickL1, getSize, insert, lookup, removeFrom,
                        setFromL3InsertTo, setL1Capacity)


class    Lru3LCacheTest a   where mkTestItem :: Int -> a
instance Lru3LCacheTest Int where mkTestItem = id

type TestFun a = forall l2 l3. Lru3LClass l2 l3 Int a => Lru3LCache l2 l3 Int a -> IO Bool


-- | GET found - L1
test1 :: Lru3LCacheTest a => TestFun a
test1 c = do
-- Level/Opearion  1  2  3    1  2  3
-- GET found       +  -  +    +  -  +
-- tick            1          2
  let k = 1
      v = mkTestItem k
  insert c k v
  t1 <- getKeyTickL1 c k
  void $ lookup c k
  t2 <- getKeyTickL1 c k
  return $ ((+1) <$> t1) == t2

-- | GET found - L2
test2 :: Lru3LCacheTest a => TestFun a
test2 c = do
-- Level/Opearion  1  2  3    1  2  3
-- GET found       -  +  +    +  +  +
-- tick               1       2  2
  let k = 1
      v = mkTestItem k
      tick = 1
  insert c 2 $ mkTestItem 2
  insert c 3 $ mkTestItem 3
  void $ storeL2 c tick k v
  void $ lookup c k
  (t1 :: Maybe Integer) <- getKeyTickL2 c k
  return $ t1 == (Just $ fromIntegral tick + 1)


-- | GET found - L3 - insert to L1
test3_1 :: TestFun a
test3_1 (setFromL3InsertTo L1Level -> c) = do
-- Level/Opearion  1  2  3    1  2  3
-- GET found       -  -  +    +  -  +
-- tick (T=curr)              T
  let k = 6
  ct1 <- getCacheTick c
  void $ lookup c k
  ct2 <- getCacheTick c
  kt  <- getKeyTickL1 c k

  return $ kt == Just ct1 && ct2 == ct1 + 1

-- | GET found - L3 - insert to L2
test3_2 :: TestFun a
test3_2 (setFromL3InsertTo L2Level -> c) = do
-- Level/Opearion  1  2  3    1  2  3
-- GET found       -  -  +    -  +  +
-- tick (T=curr)                 T
  let k = 6
  ct1 <- getCacheTick c
  void $ lookup c k
  ct2 <- getCacheTick c
  kt  <- getKeyTickL2 c k

  return $ ct1 == ct2 && kt == Just ct1

-- | GET found - L3 - insert to L2&L3
test3_3 :: TestFun a
test3_3 (setFromL3InsertTo BothLevels -> c) = do
-- Level/Opearion  1  2  3    1  2  3
-- GET found       -  -  +    +  +  +
-- tick (T=curr)              T  T
  let k = 6
  ct1  <- getCacheTick c
  void $ lookup c k
  ct2  <- getCacheTick c
  ktL1 <- getKeyTickL1 c k
  ktL2 <- getKeyTickL2 c k

  return $  (ct1 + 1) == ct2 && ktL1 == Just ct1 && ktL1 == ktL2

-- | GET not found
test4 :: TestFun a
test4 c = do
-- Level/Opearion  1  2  3    1  2  3
-- GET not found   -   -    -  -  -
-- tick
  let k = 66
  ct1  <- getCacheTick c
  void $ lookup c k
  ct2  <- getCacheTick c
  ktL1 <- getKeyTickL1 c k
  ktL2 <- getKeyTickL2 c k

  return $ ct1 == ct2 && isNothing ktL1 && isNothing ktL2

-- | EVICTED - from L1 only
test5 :: Lru3LCacheTest a => TestFun a
test5 c = do
-- Level/Opearion  1  2  3    1  2  3
-- EVICTED         +  ?  +    -  +  +
-- tick               1  ?    1
  let k = 6
      v = mkTestItem k
      n = 3
  mapM_ f [1..n]
  void $ storeL2 c 0 k v

  ct1  <- getCacheTick c
  void $ lookup c k
  ct2  <- getCacheTick c
  kt   <- getKeyTickL2 c k

  return $ ct1 == fromIntegral n && (ct1 + 1) == ct2 && kt == Just ct2
  where
    f n = insert c n $ mkTestItem n

-- | REMOVE - from L1
test6_1 :: (Lru3LCacheTest a, Store a) => TestFun a
test6_1 c = do
-- Level/Opearion  1  2  3    1  2  3
-- REMOVE       +  ?  +    -  ?  +
-- tick            ?  ?          ?
  let k = 6
  void $ setL1Capacity (k + 1) c
  mapM_ fillL1 [1..k]
  mapM_ fillL2 [1..k]

  szL11 <- getSize c
  void $ removeFrom L1Level c k
  szL12 <- getSize c
  ktL1  <- getKeyTickL1 c k
  ktL2  <- getKeyTickL2 c k

  return $ szL11 == szL12 + 1 && isNothing ktL1 && ktL2 == Just (fromIntegral szL12)
  where
    fillL1 n = insert  c                     n $ mkTestItem n
    fillL2 n = storeL2 c (fromIntegral n+10) n $ mkTestItem n

-- | REMOVE - from L2
test6_2 :: (Lru3LCacheTest a, Store a) => TestFun a
test6_2 c = do
-- Level/Opearion  1  2  3    1  2  3
-- REMOVE          ?  +  +    ?  -  +
-- tick               ?  ?       ?
  let k = 6
  void $ setL1Capacity (k + 1) c
  mapM_ fillL1 [1..k]
  mapM_ fillL2 [1..k]

  szL11  <- getSize c
  void $ removeFrom L2Level c k
  szL12  <- getSize c
  ktL1 <- getKeyTickL1 c k
  ktL2 <- getKeyTickL2 c k

  return $ szL11 == szL12 && ktL1 == Just (fromIntegral k - 1) && isNothing ktL2
  where
    fillL1 n = insert  c                     n $ mkTestItem n
    fillL2 n = storeL2 c (fromIntegral n+10) n $ mkTestItem n

-- | REMOVE - from L1&L2
test6_3 :: (Lru3LCacheTest a, Store a) => TestFun a
test6_3 c = do
-- Level/Opearion  1  2  3    1  2  3
-- REMOVE       +  +  +    -  -  +
-- tick             ?  ?
  let k = 6
  void $ setL1Capacity (k + 1) c
  mapM_ fillL1 [1..k]
  mapM_ fillL2 [1..k]

  szL11  <- getSize c
  void $ removeFrom BothLevels c k
  szL12  <- getSize c
  ktL1 <- getKeyTickL1 c k
  ktL2 <- getKeyTickL2 c k

  return $ szL11 == szL12 + 1 && isNothing ktL1 && isNothing ktL2
  where
    fillL1 n = insert  c                     n $ mkTestItem n
    fillL2 n = storeL2 c (fromIntegral n+10) n $ mkTestItem n
