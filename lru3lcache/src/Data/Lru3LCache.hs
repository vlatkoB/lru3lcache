-- | Module      : Data.Lru3LCache
--   Description : 3 level caching
--   Copyright   : (c) Vlatko Bašić, 2018
--   License     : BSD
--   Maintainer  : vlatko.basic@gmail.com
--   Stability   : beta
--   Portability : portable
--
module Data.Lru3LCache
  ( -- * Lru3LCache - cache with 3 levels

    -- $generalInfo

    -- ** Configuration of LruL3Config
    Lru3LCache
    -- *** Lru3LCache Lenses
  , initialize
  , l2Config       -- $l2Config
  , l3Config       -- $l3Config
  , evictToL2      -- $evictToL2
  , fromL3InsertTo -- $fromL3InsertTo
  , insertToL2     -- $insertTol2

    -- * Lru3LClass
  , Lru3LClass (..)

    -- * Store/remove level type
  , Lru3LLevel (..)

    -- * Functions
    -- ** Setters/getters
  , setFromL3InsertTo
  , getCacheTick
  , getKeyTickL1
  , setL1Capacity
  , getL1Capacity
  , getSize
  , lruHandle
  , lruMap

    -- ** Inserting/deleting
  , Data.Lru3LCache.insert
  , delete
  , removeFrom
  , clearCache

    -- ** Searching
  , Data.Lru3LCache.lookup
  , Data.Lru3LCache.lookupSrc

  )
where

import           Prelude                hiding (lookup)

import           Control.Arrow          (second)
import           Control.Error          (bool, isJust)
import           Control.Monad          (void, when)
import           Data.Foldable          (traverse_)
import           Data.Hashable          (Hashable)
import qualified Data.HashPSQ           as HQ
import           Data.IORef             (atomicModifyIORef', readIORef)
import           Data.LruCache          as LRU
import           Data.LruCache.Internal (LruCache (..))
import           Data.Store             (Store)
import           Data.Tuple             (swap)
import           Lens.Micro.Platform    ((&), (.~), (^.))

import           Data.Lru3LCache.Class  (Lru3LClass (..))
import           Data.Lru3LCache.Type   (Lru3LCache, Lru3LLevel (..), evictToL2,
                                         fromL3InsertTo, initialize, insertToL2, l2Config,
                                         l3Config, lruHandle, lruMap)

{- $generalInfo
Lru3LCache is a last-recently-used cache with two levels, like a CPU.
L1 is smallest and fastest and resides in memory as a 'HashMap', L2 is slower and larger,
usually on another machine and can be shared in HA environment with other peers.
Lastly, while L3 is only the data source, the last chance for finding data.

Currently, there is only 1 __L2__ backend, the __Redis__ database, fast key-value store,
and 2 __L3__ backends, PostgreSQL RDBM and ElasticSearch indexing engine.

@
mkCache :: ConnectionString -> String -> IO (PgRedCache Int Test)
mkCache connStrPG redisHost = do
  l2 <- mkL2Config
  l3 <- mkL3Lonfig
  pgRedInit l2 l3 3
  where
    mkL2Config = do
      conn <- R.checkedConnect $ R.defaultConnectInfo{connectHost = redisHost}
      return RedisCfg { uniqAppId  = "server1App"
                      , cacheId    = "mySharedCache"
                      , ttlSec     = 60
                      , connection = conn
                      , keyToBS    = BC.pack . show
                      , valToBS    = BC.pack . show
                      , valFromBS  = maybe (Left "error") Right . readMaybe . BC.pack
                      }
    mkL3Lonfig = do
      let pc = PostgresConf connStrPG 10
      pgPool <- createPoolConfig pc
      return PostgresCfg { config    = pc
                         , pool      = pgPool
                         , findByKey = P.get . intToKey
                         }

main :: IO ()
main = do
  let pgStr    = "dbname=tram host=127.0.0.1 user=memyselfandi password=hardone port=5432"
      redisSrv = "localhost"
      key      = 1

  cache <- mkCache pgConnStr redisSrv
  value <- lookup cache key
  print value
@
-}


{- $l2Config   Info about __L2__ & __L3__ configuration and lru3Lcache behaviour -}
{- $l3Config   Configuration for __L3__ level -}
{- $fromL3InsertTo When data is found in __L3__, to which level to insert it, __L1__, __L2__ or both -}
{- $insertToL2 When True, any insert to L1 activates insert to L2 -}
{- $evictToL2  When data is deleted or being evicted from __L1__, should it be stored in __L2__ -}



-- | Convenient function to set the 'fromL3InsertTo' in 'Lru3LCache'
setFromL3InsertTo :: Lru3LLevel -> Lru3LCache l2 l3 k v -> Lru3LCache l2 l3 k v
setFromL3InsertTo lvl c = c & fromL3InsertTo .~ lvl

-- | Get the value of current tick, i.e. the youngest item age
getCacheTick :: Lru3LCache l2 l3 k v -> IO Integer
getCacheTick c = fromIntegral . lruTick <$> readIORef (lruHandle c)

-- | Get the value of tick for given key
getKeyTickL1 :: (Ord k, Hashable k) => Lru3LCache l2 l3 k v -> k -> IO (Maybe Integer)
getKeyTickL1 c k =
  fmap (fromIntegral . fst). HQ.lookup k . lruQueue <$> readIORef (lruHandle c)

-- | Set new capacity for __L1__ cache
setL1Capacity :: (Hashable k, Ord k) => Int -> Lru3LCache l2 l3 k v -> IO ()
setL1Capacity n c =
  if n < 1
    then error "Capacity must be larger than 0"
    else atomicModifyIORef' (lruHandle c) $ \c' -> (go c'{lruCapacity = n},())
  where
    go = until (\LruCache{..} -> lruSize <= lruCapacity || isJust (HQ.findMin lruQueue))
               (\r -> r{ lruSize  = lruSize r - 1, lruQueue = HQ.deleteMin $ lruQueue r})

-- | Get the current __L1__ cache capacity
getL1Capacity :: Lru3LCache l2 l3 k v -> IO Int
getL1Capacity c = lruCapacity <$> readIORef (lruHandle c)

-- | Get the current cache size
getSize :: Lru3LCache l2 l3 k v -> IO Int
getSize c = lruSize <$> readIORef (lruHandle c)

-- | Insert a value with the key into the cache and, if 'insertToL2' is True, into L2
insert :: (Lru3LClass l2 l3 k v, Ord k, Hashable k)
       => Lru3LCache l2 l3 k v -> k -> v -> IO ()
insert c k v = do
  let cHndl = lruHandle c
  atomicModifyIORef' cHndl $ (, ()) . LRU.insert k v
  when (c ^. insertToL2) $ do
    tick <- lruTick <$> readIORef cHndl
    void $ storeL2 c tick k v

-- | Delete a value with key from __L1__ cache.
--   If 'evictToL2' is True, item will be stored to __L2__
--   Returns True if item was evicted to __L2__, otherwise False
delete :: (Lru3LClass l2 l3 k v,Hashable k, Ord k) => Lru3LCache l2 l3 k v -> k -> IO Bool
delete c k = do
  found <- atomicModifyIORef' (lruHandle c) $ \ioC -> do
    let hq = lruQueue ioC
    case HQ.lookup k hq of
      Nothing -> (ioC, Nothing)
      Just v  -> (ioC{ lruSize  = lruSize ioC - 1
                     , lruQueue = HQ.delete k hq
                     }
                 , Just v
                 )
  case found of
    Just (r,v) -> when (c ^. evictToL2) (void $ storeL2 c r k v) >> return True
    _          ->                                                   return False

-- | Delete a value with key from given cache level
removeFrom :: (Lru3LClass l2 l3 k v, Show k, Store v, Hashable k, Ord k)
           => Lru3LLevel ->  Lru3LCache l2 l3 k v -> k -> IO Int
removeFrom remOpt c k = case remOpt of
  L1Level    -> bool 1 0 <$> delete c k
  L2Level    -> removeL2 c k
  L3Level    -> return 0
  BothLevels -> (+) <$> removeFrom L1Level c k <*> removeFrom L2Level c k

-- | Remove all items from __L1__ cache
clearCache :: Lru3LCache l2 l3 k v -> IO ()
clearCache c = atomicModifyIORef' (lruHandle c) $ (,()) . empty . lruCapacity

-- | Lookup a value by its key. Searches first in __L1__ level, if not found in __L2__,
--   and if not found then in __L3__.
--   If the key was found in  __L3__, it'll be inserted to __L1__, __L2__ or both levels,
--   depending on the 'fromL3InsertTo' value.
lookup :: (Lru3LClass l2 l3 k v, Hashable k, Ord k)
       => Lru3LCache l2 l3 k v -> k -> IO (Maybe v)
lookup c k = fmap fst <$> lookupSrc c k

lookupSrc :: (Lru3LClass l2 l3 k v, Hashable k, Ord k)
          => Lru3LCache l2 l3 k v -> k -> IO (Maybe (v, Lru3LLevel))
lookupSrc c k = l1 <<|>> l2 <<|>> l3
  where
    cHndl = lruHandle c
    (<<|>>) f g = f >>= maybe g (return . Just)
    l1 = atomicModifyIORef' cHndl
      (\r -> maybe (r,Nothing) (second (Just . (,L1Level)) . swap) $ LRU.lookup k r)

    l2 = do
      LruCache{..} <- readIORef cHndl
      lookupL2 c lruTick k >>= \case
        Nothing         -> return Nothing
        Just (v,l2Tick) -> do
          let minTick = maybe 0 snd3 $ HQ.findMin lruQueue
          when (minTick <= l2Tick) . void $ insertL1 v
          return $ Just (v, L2Level)

    l3 = lookupL3 c k >>= \case
        Nothing -> return Nothing
        Just v  -> do
          void $ case c ^. fromL3InsertTo of
            L1Level    -> insertL1 v
            L2Level    -> insertL2 k v
            L3Level    -> return Nothing
            BothLevels -> insertL2 k v >> insertL1 v
          return $ Just (v, L3Level)

    insertL1 v = do
      atomicModifyIORef' cHndl (swap . insertView k v)
        >>= traverse_ (when (c ^. evictToL2) . void . insertL2 k . snd)
      return $ Just v

    insertL2 k' v = do
      tick <- lruTick <$> readIORef cHndl
      storeL2 c tick k' v


-- | Second of triple
{-# INLINE snd3 #-}
snd3 :: (a,b,c) -> b
snd3 (_,x,_) = x
