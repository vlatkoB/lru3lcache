module FileSystemSpec (main, spec) where

import           Prelude                       hiding (lookup)

import           Control.Monad                 (void)
import           Data.Char                     (isDigit)
import           Data.String                   (IsString)
import           Data.String.Conv              (toS)
import           Database.Redis                (ConnectInfo (..), PortID (..))
import qualified Database.Redis                as R
import           Lens.Micro.Platform           ((&), (.~), (^.))
import           Text.Read                     (readMaybe)

import           Test.Hspec                    (Spec, describe, hspec, it, shouldReturn)


import           Data.Lru3LCache               (Lru3LLevel (..), fromL3InsertTo, l2Config)
import           Data.Lru3LCache.Backend.Redis (RedisCfg (..))
import           Data.Lru3LCache.FSRedis       (FSRedCache, FileSystemCfg (..), fsRedInit)
import           Data.Lru3LCache.Test.Tests


host :: IsString s => s
host = "localhost"

baseDir :: FilePath
baseDir = "test/FSRedData"

main :: IO ()
main = hspec spec

spec :: Spec
spec = do
  describe "NoL2: Cache 3 Levels" $ mapM_ runCacheTest [1,2,31,32,33,4,5,61,62,63]

  where
    runCacheTest n = it ("Test " <> show n) $ runTest n `shouldReturn` True


-- | Run specified test on Redis L2 cache with no L3 backend.
runTest :: Int -> IO Bool
runTest n = do
  c <- mkFSCacheTest baseDir host
  void $ R.runRedis (connection $ c ^. l2Config) R.flushall
  go c n

  where
    go cache test = case test of
      1  -> test1   cache
      2  -> test2   cache
      31 -> test3_1 cache
      32 -> test3_2 cache
      33 -> test3_3 cache
      4  -> test4   cache
      5  -> test5   cache
      61 -> test6_1 cache
      62 -> test6_2 cache
      63 -> test6_3 cache
      _  -> return False

-- | Create Lru3LCache of FileSystem and Redis backends combination
mkFSCacheTest :: FilePath -> String -> IO (FSRedCache Int Int)
mkFSCacheTest path redisHost = do
  l2  <- mkRedisCfg redisHost
  c3l <- fsRedInit l2 fsConfig 3
  pure $ c3l & fromL3InsertTo .~ L2Level
  where
    fsConfig = FileSystemCfg
      { fsRoot      = path
      , keyToPath   = show
      , fileToVal   = readMaybe . filter isDigit . toS
      , maxFileSize = 15
      }

-- | Create L2Level 'Redis' cache
mkRedisCfg :: (Show k, Show v, Read v) => String -> IO (RedisCfg k v)
mkRedisCfg redisHost= do
  conn <- R.checkedConnect redConnInfo
  return RedisCfg { uniqAppId  = "cachePrefix"
                  , cacheId    = "AppIdent"
                  , ttlSec     = 200
                  , connection = conn
                  , keyToBS    = toS . show
                  , valToBS    = toS . show
                  , valFromBS  = maybe (Left "error") Right . readMaybe . toS
                  }
  where
    redConnInfo  = R.defaultConnectInfo
      { connectHost           = redisHost
      , connectPort           = PortNumber 6379 -- Redis default port
      , connectAuth           = Nothing         -- No password
      , connectDatabase       = 0               -- SELECT database 0
      , connectMaxConnections = 50              -- Up to 50 connections
      , connectMaxIdleTime    = 30              -- Keep open for 30 seconds
      , connectTimeout        = Nothing         -- Don't add timeout logic
      }
