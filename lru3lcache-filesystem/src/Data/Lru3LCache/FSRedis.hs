{-# OPTIONS_GHC -fno-warn-orphans #-}
module Data.Lru3LCache.FSRedis
  ( FSRedCache
  , RedisCfg (..)
  , FileSystemCfg (..)
  , fsRedInit
  )
where

import Data.Lru3LCache                    (Lru3LCache, Lru3LClass (..), initialize)
import Data.Lru3LCache.Backend.FileSystem (FileSystemCfg (..), fsFind)
import Data.Lru3LCache.Backend.Redis      (RedisCfg (..), redisGetKeyTick, redisIncrTick,
                                           redisLookup, redisRemove, redisSetTick,
                                           redisStore)


-- | Convenient type for Redis-FileSystem backend combo
type FSRedCache k v = Lru3LCache RedisCfg FileSystemCfg k v

instance Lru3LClass RedisCfg FileSystemCfg k v where
  lookupL2     = redisLookup
  storeL2      = redisStore
  removeL2     = redisRemove
  incrTickL2   = redisIncrTick
  getKeyTickL2 = redisGetKeyTick
  setTickL2    = redisSetTick
  lookupL3     = fsFind


-- | Initialize FSRedis
fsRedInit :: RedisCfg k v -> FileSystemCfg k v -> Int -> IO (FSRedCache k v)
fsRedInit = initialize
