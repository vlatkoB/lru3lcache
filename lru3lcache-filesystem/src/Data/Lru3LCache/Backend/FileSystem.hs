module Data.Lru3LCache.Backend.FileSystem
  ( FileSystemCfg (..)
  , fsFind
  )
where

import Prelude             hiding (readFile)

import Data.Bool           (bool)
import Data.ByteString     (ByteString, readFile)
import Data.String.Conv    (toS)
import Lens.Micro.Platform ((^.))
import System.Directory    (canonicalizePath, doesFileExist, getFileSize)
import System.FilePath     ((</>))


import Data.Lru3LCache     (Lru3LCache, l3Config)


-- | Configuration for FileSystem backend based on 'Database.FileSystem'
data FileSystemCfg k v = FileSystemCfg
  { fsRoot      :: FilePath               -- ^ Root directory of stored files
  , keyToPath   :: k          -> FilePath -- ^ Convert key to file path
  , fileToVal   :: ByteString -> Maybe v  -- ^ Convert file content to a value
  , maxFileSize :: Integer                -- ^ File larger than this is NOT valid
  }

-- | Search for a file by filename and extension
fsFind :: Lru3LCache l2 FileSystemCfg k v -> k -> IO (Maybe v)
fsFind c fNm = do
  fullPath <- canonicalizePath $ fsRoot </> keyToPath fNm
  doesFileExist fullPath >>= \case
    False -> pure Nothing
    True  -> bool (pure Nothing)
                  (makeValue fullPath)
                . (maxFileSize > )
               =<< getFileSize fullPath

  where
    FileSystemCfg{..} = c ^. l3Config
    makeValue f       = fileToVal . toS <$> readFile f
