-- | Module      : Data.Lru3LCache.Backend.Redis
--   Description : Redis backend for Lru3LCache
--   Copyright   : (c) Vlatko Bašić, 2018
--   License     : BSD
--   Maintainer  : vlatko.basic@gmail.com
--   Stability   : beta
--   Portability : portable
--
module Data.Lru3LCache.Backend.Redis
  ( RedisCfg (..)
  , redisMkCacheKey
  , redisMkTickField
  , redisGetKeyTick
  , redisIncrTick
  , redisSetTick
  , redisRemove
  , redisStore
  , redisLookup
  , tryMultiRedis
  , tryRedis
  )
where

import           Control.Exception.Base (IOException, catch)
import           Control.Monad          (void)
import           Data.ByteString        (ByteString)
import           Data.Int               (Int64)
import           Data.Maybe             (fromMaybe)
import           Data.String.Conv       (toS)
import           Data.Text              (Text)
import qualified Database.Redis         as R
import           Lens.Micro.Platform    ((^.))
import           Text.Read              (readMaybe)


import           Data.Lru3LCache        (Lru3LCache, l2Config)


-- | Configuration for Redis backend based on 'Database.Redis'
data RedisCfg k v = RedisCfg
  { -- | Unique identification of the application using it.
    --   Two apps in HA link should have different @uniqAppId@.
    --   IP address is good unique name.
    uniqAppId  :: ByteString
    -- | Prefix added to the key storing cached items.
    --   Two apps in HA link should have the same @cacheId@, so the cache can be shared.
  , cacheId    :: ByteString
  , ttlSec     :: Integer                     -- ^ Time-To-Live in seconds
  , connection :: R.Connection                -- ^ Redis connection configuration
  , keyToBS    :: k          -> ByteString    -- ^ encode a key   to   'ByteString'
  , valToBS    :: v          -> ByteString    -- ^ encode a value to   'ByteString'
  , valFromBS  :: ByteString -> Either Text v -- ^ decode a value from' ByteString'
  }


-- | Converts the key to 'ByteString'
redisMkCacheKey :: Lru3LCache RedisCfg l3 k v -> k -> ByteString
redisMkCacheKey c k = cacheId (c ^. l2Config) <> ":" <> keyToBS (c ^. l2Config) k

-- | Creates a key for storing ticks specific to current application, based on @uniqAppId@
redisMkTickField :: Lru3LCache RedisCfg l3 k v -> ByteString
redisMkTickField c = uniqAppId (c ^. l2Config) <> ":tick"

-- | Get value of tick for given key
redisGetKeyTick :: Lru3LCache RedisCfg l3 k v -> k -> IO (Maybe Integer)
redisGetKeyTick c k =
  (\case Right (Right (Just bs)) -> readMaybe $ toS bs
         _                       -> Nothing
    ) <$> tryRedis (getConn c) (R.hget (redisMkCacheKey c k) (redisMkTickField c))

-- | Increment the value of tick for the given key
redisIncrTick :: Lru3LCache RedisCfg l3 k v -> k -> IO ()
redisIncrTick c k =
  void $ tryRedis (getConn c) (R.hincrby (redisMkCacheKey  c k) (redisMkTickField c) 1)

-- | Set the value of tick for the given key
redisSetTick :: Lru3LCache RedisCfg l3 k v -> Int64 -> k -> IO ()
redisSetTick c tick k =
  void $ tryRedis (getConn c) (R.hset (redisMkCacheKey  c k)
                                      (redisMkTickField c)
                                      (toS $ show tick)
                              )

-- | Remove given key from Redis
redisRemove :: Lru3LCache RedisCfg l3 k v -> k -> IO Int
redisRemove c k =
  (\case Right (Right n) | n > 0 -> 1
         _                       -> 0
  ) <$> tryRedis (getConn c ) (R.del [redisMkCacheKey c k])

-- | Store key/value to Redis
redisStore :: forall l3 k v. Lru3LCache RedisCfg l3 k v -> Int64 -> k -> v -> IO (Maybe v)
redisStore c tick k v = do
  r <- tryMultiRedis connection $ do
    s <- R.hmset rKey [ (redisMkTickField c, toS (show tick))
                      , ("content", valToBS v)
                      ]
    b <- R.expire rKey ttlSec
    return $ (,) <$> s <*> b
  return $ case r of
    R.TxSuccess (R.Ok, True) -> Just v
    _                        -> Nothing
  where
    rKey         = redisMkCacheKey c k
    RedisCfg{..} = c ^. l2Config

-- | Search for a key in Redis
redisLookup :: Lru3LCache RedisCfg l3 k v -> Int64 -> k -> IO (Maybe (v, Int64))
redisLookup c tick k = do
  r <- tryMultiRedis connection $ do
    fs  <- R.hgetall (redisMkCacheKey c k)
    void $ R.hset    (redisMkCacheKey c k) (redisMkTickField c) (toS $ show tick)
    return fs
  case r of
    R.TxSuccess xs -> case valFromBS <$> Prelude.lookup "content" xs of
      Just (Right v) -> do
        let oldTick = fromMaybe 0
                   $  readMaybe
                   .  toS
                  =<< Prelude.lookup (redisMkTickField c) xs
        return $ Just (v, oldTick)
      Nothing -> redisRemove c k >> return Nothing
      _       ->                    return Nothing
    _ -> return Nothing

  where
    RedisCfg{..} = c ^. l2Config

-- | Try to execute Redis multi-action
tryMultiRedis :: R.Connection -> R.RedisTx (R.Queued a) -> IO (R.TxResult a)
tryMultiRedis c a =
  R.runRedis c (R.multiExec a)
    `catch` (\(e::IOException)               -> return . R.TxError $ show e)
    `catch` (\(e::R.ConnectionLostException) -> return . R.TxError $ show e)

-- | Try to execute Redis action
tryRedis :: R.Connection -> R.Redis a -> IO (Either String a)
tryRedis c a =
  (Right <$> R.runRedis c a)
   `catch` (\(e::IOException)               -> return . Left $ show e)
   `catch` (\(e::R.ConnectionLostException) -> return . Left $ show e)

-- | Extract Redis connection from  Lru3LCache
getConn :: Lru3LCache RedisCfg l3 k v -> R.Connection
getConn c = connection $ c ^. l2Config
