{-# OPTIONS_GHC -fno-warn-orphans #-}
module RedisSpec (main, spec) where

import           Prelude                       hiding (lookup)

import           Data.String                   (IsString)
import           Data.String.Conv              (toS)
import           Database.Redis                (ConnectInfo (..), PortID (..))
import qualified Database.Redis                as R
import           Lens.Micro.Platform           ((&), (.~))
import           Text.Read                     (readMaybe)

import           Test.Hspec                    (Spec, describe, hspec, it, shouldReturn)

import           Data.Lru3LCache
    (Lru3LCache, Lru3LClass (..), Lru3LLevel (..), fromL3InsertTo, initialize)
import           Data.Lru3LCache.Backend.Redis
    ( RedisCfg (..)
    , redisGetKeyTick
    , redisIncrTick
    , redisLookup
    , redisRemove
    , redisSetTick
    , redisStore
    )
import           Data.Lru3LCache.Class         (NoL3 (..), noL3lookupL3)
import           Data.Lru3LCache.Test.Tests



host :: IsString s => s
host = "localhost"

main :: IO ()
main = hspec spec

type RedisNoL3Cache k v = Lru3LCache RedisCfg NoL3 k v

instance Lru3LClass RedisCfg NoL3 k v where
  lookupL2     = redisLookup
  storeL2      = redisStore
  removeL2     = redisRemove
  incrTickL2   = redisIncrTick
  getKeyTickL2 = redisGetKeyTick
  setTickL2    = redisSetTick
  lookupL3     = noL3lookupL3

spec :: Spec
spec = do
  describe "NoL2: Cache 3 Levels" $ mapM_ runCacheTest [1,2,4,5,61,62,62]

  where
    runCacheTest n = it ("Test " <> show n) $ runTest n `shouldReturn` True


-- | Run specified test on Redis L2 cache with no L3 backend.
runTest :: Int -> IO Bool
runTest n = flip go n =<< mkLru3LCacheTest host

  where
    go cache test = case test of
      1  -> test1   cache
      2  -> test2   cache
      31 -> test3_1 cache
      32 -> test3_2 cache
      33 -> test3_3 cache
      4  -> test4   cache
      5  -> test5   cache
      61 -> test6_1 cache
      62 -> test6_2 cache
      63 -> test6_3 cache
      _  -> return False

mkLru3LCacheTest :: String -> IO (RedisNoL3Cache Int Int)
mkLru3LCacheTest redisHost = do
  l2 <- mkRedisCfg redisHost
  l3 <- pure NoL3
  c3l <- initialize l2 l3 3
  return $ c3l & fromL3InsertTo .~ L2Level

-- | Create L2Level 'Redis' cache
mkRedisCfg :: (Show k, Show v, Read v) => String -> IO (RedisCfg k v)
mkRedisCfg redisHost= do
  conn <- R.checkedConnect redConnInfo
  return RedisCfg { uniqAppId  = "cachePrefix"
                  , cacheId    = "AppIdent"
                  , ttlSec     = 200
                  , connection = conn
                  , keyToBS    = toS . show
                  , valToBS    = toS . show
                  , valFromBS  = maybe (Left "error") Right . readMaybe . toS
                  }
  where
    redConnInfo  = R.defaultConnectInfo
      { connectHost           = redisHost
      , connectPort           = PortNumber 6379 -- Redis default port
      , connectAuth           = Nothing         -- No password
      , connectDatabase       = 0               -- SELECT database 0
      , connectMaxConnections = 50              -- Up to 50 connections
      , connectMaxIdleTime    = 30              -- Keep open for 30 seconds
      , connectTimeout        = Nothing         -- Don't add timeout logic
      }
