#!/bin/bash

echo Starting services
start-all-services.sh

echo Preparing DB
pg-add-user.sh        "vlatko"   "pero"
pg-create-db-owner.sh "lru-test" "vlatko"


echo Cloning repo
git clone https://gitlab.com/vlatkoB/lru3lcache.git


echo Building and testing repo
cd lru3lcache
run-these-tests.sh FileSystem Redis Postgres ElasticSearch
